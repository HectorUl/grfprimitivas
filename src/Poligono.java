
import java.awt.Color;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author loslo
 */
public class Poligono extends Figura {
    Point punto1;
    Point punto2;  
    int gr;
    int lados;     
     Poligono(Point _p1, Point _p2,int lados,int g, Color _color) {      
         punto1 = new Point(_p1.x,_p1.y);
         punto2 = new Point(_p2.x,_p2.y);
         gr =g;
         this.lados = lados;
         color  = _color;
    }  
}
