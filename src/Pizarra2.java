/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gmendez
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Pizarra2 extends javax.swing.JFrame {
    /**
     * Creates new form Pizarra2
     */

    static final int LINEA     = 0;
    static final int TRIANGULO = 1;
    static final int TRIANGULOR = 2;
    static final int CUADRADO  = 3;
    static final int CUADRADOR  = 4;
    static final int CIRCULO  = 5;
    static final int CIRCULOR  = 6;
    static final int ELIPSE  = 7;
    static final int ELIPSER  = 8;
    static final int POLIGONO  = 9;
    static final int POLIGONOR  = 10;
    
    static final int ANCHO     = 640;
    static final int ALTO      = 480;
    
    Raster  raster;    
    
    Point   p1, p2, p3,p4;
    boolean bP1=false, bP2=false, bP3=false,bP4 = false;
    int     figura = LINEA;    
    int la = 0;     
    Matrix matrix;
    int grados = 0;
    JPanel      panelRaster;
    JPanel      panelControles;
    JPanel      panelFiguras;
    JScrollPane scrollFiguras;
    
    JList             listFiguras;
    DefaultListModel  listModel;    
    ArrayList<Figura> aFiguras;
    
    JButton       btnColor;
    JToggleButton rbLinea;
    JToggleButton rbTriang;
    JToggleButton rbTriangR;
    JToggleButton rbCuad;
    JToggleButton rbCuadR;
    JToggleButton rbCirc;
    JToggleButton rbCircR;
    JToggleButton rbElip;
    JToggleButton rbElipR;
    JToggleButton rbPolig;
    JToggleButton rbPoligR;
    ButtonGroup   bg;
    
    JTextField tfLados;
    JLabel lLados;
    
    
    JButton btnTrasladar;
    JButton btnEscalar;
    JButton btnRotar;

    JTextField campoTX;
    JTextField campoTY;
    JTextField campoEX;
    JTextField campoEY;
    JTextField campoR;

    
    
    Color         color;
    JColorChooser colorChooser;
    JButton       btnGuardarRast;
    JButton       btnGuardarVect;
    JButton btnAbrirVect;
    JButton btnBorrar;
    JButton btnEliminarFigura;
           
    public Pizarra2() { 
        
        p1 = new Point();
        p2 = new Point();        
        p3 = new Point();
        p4 = new Point();
        
        bP1 = false; bP2 = false; bP3 = false; bP4 = false;
        
        raster = new Raster(ANCHO,ALTO);        
        matrix = new Matrix();
        panelRaster = new MyPanel(raster);
        
        panelControles = new JPanel();
        panelControles.setLayout(new BoxLayout(panelControles,BoxLayout.Y_AXIS));
        
                
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        this.setLayout(new BorderLayout());       
        
        color = Color.black;
        
        btnGuardarRast = new JButton("Guardar");   
        btnColor       = new JButton("Color");
        
        btnColor.setBorderPainted(false);
        btnColor.setFocusPainted(false);
                
        btnColor.setBackground(color);
        btnColor.setForeground(color);
        
         campoTX = new JTextField(3);
        campoTY = new JTextField(3);
        campoEX = new JTextField(3);
        campoEY = new JTextField(3);
        campoR = new JTextField(3);

        
        tfLados = new JTextField();                      
        lLados = new JLabel("lados: ");      
        rbLinea  = new JToggleButton("Linea");
        rbTriang = new JToggleButton("Triangulo");
        rbTriangR = new JToggleButton("TrianguloR");
        rbCuad   = new JToggleButton("Cuadrado");
        rbCuadR   = new JToggleButton("CuadradoR");
        rbCirc  = new JToggleButton("Circulo");
        rbCircR  = new JToggleButton("CirculoR");
        rbElip  = new JToggleButton("Elipse");
        rbElipR  = new JToggleButton("ElipseR");
        rbPolig  = new JToggleButton("Poligono");
        rbPoligR  = new JToggleButton("PoligonoR");
        
        btnTrasladar = new JButton("Trasladar");
        btnTrasladar.setName("trasladar");
        btnEscalar = new JButton("Escalar");
        btnEscalar.setName("escalar");
        btnRotar = new JButton("Rotar");
        btnRotar.setName("rotar");
        
        bg = new ButtonGroup();
        
        rbLinea.setSelected(true);
        bg.add(rbLinea);
        bg.add(rbTriang);
        bg.add(rbTriangR);
        bg.add(rbCuad);
        bg.add(rbCuadR);
        bg.add(rbCirc);
        bg.add(rbCircR);
        bg.add(rbElip);
        bg.add(rbElipR);        
        bg.add(rbPolig);
        bg.add(rbPoligR);
        
        this.panelRaster.setBackground(Color.white);
        this.add(panelRaster,BorderLayout.CENTER);
        
        this.panelControles.add(rbLinea);
        this.panelControles.add(rbTriang);
        this.panelControles.add(rbTriangR);        
        this.panelControles.add(rbCuad);
        this.panelControles.add(rbCuadR);
        this.panelControles.add(rbCirc);
        this.panelControles.add(rbCircR);
        this.panelControles.add(rbElip);
        this.panelControles.add(rbElipR);
        this.panelControles.add(rbPolig);
        this.panelControles.add(rbPoligR);
        this.panelControles.add(new JSeparator());
       this.panelControles.add(lLados);
        this.panelControles.add(tfLados);        
       this.panelControles.add(new JSeparator());
         this.panelControles.add(btnTrasladar);
        JPanel panelT = new JPanel();
        panelT.setLayout(new FlowLayout());
        panelT.add(campoTX);
        panelT.add(campoTY);
        this.panelControles.add(panelT);
        this.panelControles.add(new JSeparator());
        this.panelControles.add(btnEscalar);
        JPanel panelE = new JPanel();
        panelE.setLayout(new FlowLayout());
        panelE.add(campoEX);
        panelE.add(campoEY);
        this.panelControles.add(panelE);
        this.panelControles.add(new JSeparator());
        this.panelControles.add(btnRotar);
        JPanel panelR = new JPanel();
        panelR.setLayout(new FlowLayout());
        panelR.add(campoR);
        this.panelControles.add(panelR);
        this.panelControles.add(new JSeparator());
        this.panelControles.add(btnColor);
        
        
                
        this.panelControles.add(btnColor);

        this.panelControles.add(btnGuardarRast);
        
        // Ahora el pane de figuras
        
        btnGuardarVect = new JButton("Guardar");
          btnAbrirVect = new JButton("Abrir Vector");

        scrollFiguras = new JScrollPane();
        panelFiguras  = new JPanel();
        
        panelFiguras.setLayout(new BoxLayout(panelFiguras,BoxLayout.Y_AXIS));       
                        
        listFiguras = new JList(); 
        listFiguras.setModel(new DefaultListModel());
        listModel = (DefaultListModel) listFiguras.getModel();
        
        scrollFiguras.setViewportView(listFiguras);
                
        scrollFiguras.setPreferredSize(new Dimension(50,100));
        btnEliminarFigura = new JButton("Eliminar Figura");
        panelFiguras.add(btnEliminarFigura);
        panelFiguras.add(scrollFiguras);
        panelFiguras.add(new JSeparator());
        panelFiguras.add(btnGuardarVect);
        panelFiguras.add(btnAbrirVect);
        btnBorrar = new JButton("Borrar Raster");
        panelFiguras.add(btnBorrar);
        
        aFiguras = new ArrayList<Figura>();        
                
        this.add(panelFiguras,BorderLayout.EAST);
        this.add(panelControles,BorderLayout.WEST);
        grados();
        this.panelRaster.addMouseListener(new MouseAdapter(){
                                        @Override
                                        public void mouseClicked(MouseEvent evt) {
                                                jPanel1MouseClicked(evt);
                                        }                               
                                }); 
        
        
        this.panelRaster.addKeyListener(new KeyAdapter(){
                                       @Override
                                       public void keyReleased(KeyEvent ke){
                                             
                                                 jPanel1KeyReleased(ke);
                                             
                                       }
                                 });        
        
          this.btnTrasladar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Figura figura = aFiguras.get(listFiguras.getSelectedIndex());
                String tipoTransf = ((JButton) e.getSource()).getName();
                Point[] puntos = getPuntosFigura(figura);
                ArrayList<double[]> list_pprima = aplicarTransformacion(tipoTransf, puntos);
                dibujarFigura(list_pprima, figura);
            }
        });

        this.btnEscalar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Figura figura = aFiguras.get(listFiguras.getSelectedIndex());
                String tipoTransf = ((JButton) e.getSource()).getName();               
                Point[] puntos = getPuntosFigura(figura);
                ArrayList<double[]> list_pprima = aplicarTransformacion(tipoTransf, puntos);
                dibujarFigura(list_pprima, figura);
            }
        });

        this.btnRotar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Figura figura = aFiguras.get(listFiguras.getSelectedIndex());
                String tipoTransf = ((JButton) e.getSource()).getName(); 
                
                if(figura instanceof PoligonoR){
                    PoligonoR l = (PoligonoR) figura;
                    grados();
                    dibujarPoligonoRegularR(l.punto1,l.punto2,l.lados,grados,l.color);
                    String linea = String.format("PoR,%.0f,%.0f,%.0f,%.0f,%d,%d,%x\n", l.punto1.getX(), l.punto1.getY(),                            
                    l.punto2.getX(), l.punto2.getY(),la,grados,
                    l.color.getRGB());  
                    listModel.addElement(linea);
                     grados = 90;
                }else{
                    
                Point[] puntos = getPuntosFigura(figura);
                ArrayList<double[]> list_pprima = aplicarTransformacion(tipoTransf, puntos);
                dibujarFigura(list_pprima, figura);
            }}
        });

        
        this.btnColor.addActionListener(new ActionListener(){
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                        color = JColorChooser.showDialog(null,"Seleccione un color",color);
                                                        btnColor.setBackground(color);
                                                }
                                });

         this.btnEliminarFigura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int figuraSeleccionada = listFiguras.getSelectedIndex();
                listModel.remove(figuraSeleccionada);
                aFiguras.remove(figuraSeleccionada);
                guardarVectores();
                listModel.clear();
                aFiguras.clear();
                borrarFiguras();
                openVectors("vectores.txt");
                redibujar();
            }
        });

        
        this.btnGuardarRast.addActionListener(new ActionListener(){
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                        guardarImagen();
                                                }
                                });

        this.btnGuardarVect.addActionListener(new ActionListener(){
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                        guardarVectores();
                                                }
                                });        
        
           this.btnAbrirVect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                abrirVectores();
            }
        });

        this.btnBorrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                borrarFiguras();
            }
        });

        
        this.setVisible(true);
        this.pack();
        
    }
   
     
     
    public static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
   
    
    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }    
    
//    public void guardarImagen(){
//        
//        BufferedImage img = toBufferedImage(raster.toImage(this));        
//        try {                        
//            File outputfile = new File("saved.png");
//            ImageIO.write(img, "png", outputfile);
//        } catch (IOException e) {
//          
//        }                
//    }
    //-----------------------------------
        public void dibujarFigura(ArrayList<double[]> listpprima, Figura figura) {
        String linea;
        Point p1, p2, p3, p4;
        if (figura instanceof Linea) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            this.dibujarLinea(p1, p2, color);

            Linea l = new Linea(p1, p2, color);

            linea = String.format("L,%.0f,%.0f,%.0f,%.0f,%x\n", l.punto1.getX(), l.punto1.getY(),
                    l.punto2.getX(), l.punto2.getY(),
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

        if (figura instanceof TrianguloR) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);
            double[] pp3 = listpprima.get(2);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            p3 = new Point((int) pp3[0], (int) pp3[1]);
            this.dibujarTrianguloR(p1, p2, p3, color);
          
            TrianguloR l = new TrianguloR(p1, p2, p3, color);

            linea = String.format("TR,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x, p1.y,
                    p2.x,p2.y,p3.x,p3.y,
                    color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

        if (figura instanceof Triangulo) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);
            double[] pp3 = listpprima.get(2);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            p3 = new Point((int) pp3[0], (int) pp3[1]);
            this.dibujarTriangulo(p1, p2, p3, color);

            Triangulo l = new Triangulo(p1, p2, p3, color);

            linea = String.format("T,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x,p1.y,
                    p2.x, p2.y,p3.x, p3.y,
                    color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

        if (figura instanceof CirculoR) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            this.dibujarCirculoR(p1, p2, color);

            CirculoR l = new CirculoR(p1, p2, color);

            linea = String.format("CiR,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x, p1.y,
                    p2.x, p2.y,
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

        if (figura instanceof Circulo) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            this.dibujarCirculo(p1, p2, color);

            Circulo l = new Circulo(p1, p2, color);

            linea = String.format("Ci,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x, p1.y,
                    p2.x, p2.y,
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

         if (figura instanceof ElipseR) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);                        
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            
            this.dibujarElipseR(p1, p2, color);

            ElipseR l = new ElipseR(p1, p2, color);

            linea = String.format("ElR,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x, p1.y,
                    p2.x, p2.y,
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }
         
        if (figura instanceof Elipse) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);                          
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
            
            this.dibujarElipse(p1, p2, color);

            Elipse l = new Elipse(p1, p2, color);

            linea = String.format("El,%.0f,%.0f,%.0f,%.0f,%x\n", p1.x, p1.y,
                    p2.x, p2.y,
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }

        if (figura instanceof Cuadrado) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1); 
                double[] pp3 = listpprima.get(2);
            double[] pp4 = listpprima.get(3);
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);
             p3 = new Point((int) pp3[0], (int) pp3[1]);
            p4 = new Point((int) pp4[0], (int) pp4[1]); 
            this.dibujarCuadrado(p1, p2, color);

            Cuadrado l = new Cuadrado(p1, p2,p3,p4, color);

            linea = String.format("Cu,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n", l.punto1.getX(), l.punto1.getY(),
                    l.punto2.getX(), l.punto2.getY(),l.punto3.getX(), l.punto3.getY(),
                    l.punto4.getX(), l.punto4.getY(),
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }
         if (figura instanceof CuadradoR) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);   
             double[] pp3 = listpprima.get(2);
            double[] pp4 = listpprima.get(3); 
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);   
            p3 = new Point((int) pp3[0], (int) pp3[1]);
            p4 = new Point((int) pp4[0], (int) pp4[1]); 
            this.dibujarCuadradoR(p1, p2, color);

            CuadradoR l = new CuadradoR(p1,p2,p3,p4, color);

            linea = String.format("CuR,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n", l.punto1.getX(), l.punto1.getY(),
                    l.punto2.getX(), l.punto2.getY(),l.punto3.getX(), l.punto3.getY(),
                    l.punto4.getX(), l.punto4.getY(),
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }
//          if (figura instanceof PoligonoR) {
//            double[] pp1 = listpprima.get(0);
//            double[] pp2 = listpprima.get(1);         
//            p1 = new Point((int) pp1[0], (int) pp1[1]);
//            p2 = new Point((int) pp2[0], (int) pp2[1]);           
//            this.dibujarPoligonoRegularR(p1, p2,la,grados, color);
//
//            PoligonoR l = new PoligonoR(p1, p2,la,grados,color);
//
//            linea = String.format("PoR,%.0f,%.0f,%.0f,%.0f,%d,%x\n", l.punto1.getX(), l.punto1.getY(),
//                    l.punto2.getX(), l.punto2.getY(),la,grados,
//                    l.color.getRGB());            
//            aFiguras.add(l);
//            listModel.addElement(linea);
//        }
           if (figura instanceof Poligono) {
            double[] pp1 = listpprima.get(0);
            double[] pp2 = listpprima.get(1);         
            p1 = new Point((int) pp1[0], (int) pp1[1]);
            p2 = new Point((int) pp2[0], (int) pp2[1]);           
            this.dibujarPoligonoRegular(p1, p2,la,grados, color);

            Poligono l = new Poligono(p1, p2,la,grados, color);
            
            linea = String.format("Po,%.0f,%.0f,%.0f,%.0f,%d,%d,%x\n", l.punto1.getX(), l.punto1.getY(),
                    l.punto2.getX(), l.punto2.getY(),la,grados,
                    l.color.getRGB());

            aFiguras.add(l);
            listModel.addElement(linea);
        }
           grados = 90;
        this.repaint();
    }

    public ArrayList<double[]> aplicarTransformacion(String tipoTrans, Point[] puntos) {

        matrix.loadIdentity();

        switch (tipoTrans) {
            case "trasladar":
                int dx = Integer.parseInt(campoTX.getText());
                int dy = Integer.parseInt(campoTY.getText());
                matrix.traslacion(dx, dy);
                break;
            case "escalar":
                int ex = Integer.parseInt(campoEX.getText());
                int ey = Integer.parseInt(campoEY.getText());
                matrix.escalarXY(ex, ey);
                break;
            case "rotar":
                int r = Integer.parseInt(campoR.getText());
                if (r < 0) {
                    r *= -1;
                } else {
                    r *= -1;
                }
                matrix.rotacion(r);
        }

        ArrayList<double[]> listPPrima = new ArrayList<>();
        for (int i = 0; i < puntos.length; i++) {
            double[] punto = {puntos[i].x, puntos[i].y, 1};
            double[] pprima = matrix.pprima(punto);
            listPPrima.add(pprima);
        }

        ArrayList<double[]> listAuxPPrima = new ArrayList<>();
        if (tipoTrans.equals("rotar")) {
            double[] p1 = {puntos[0].x - listPPrima.get(0)[0], puntos[0].y - listPPrima.get(0)[1], 1};
            matrix.loadIdentity();
            matrix.traslacion((int) p1[0], (int) p1[1]);
            for (int i = 0; i < listPPrima.size(); i++) {
                double[] punto = {listPPrima.get(i)[0], listPPrima.get(i)[1], 1};
                double[] pprima = 
                        matrix.pprima(punto);
                listAuxPPrima.add(pprima);
            }
            return listAuxPPrima;
        }

        return listPPrima;

    }

    public Point[] getPuntosFigura(Figura figura) {
        Point[] puntos = null;
        if (figura instanceof Linea) {
            Linea l = (Linea) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;                     
        }

        if (figura instanceof TrianguloR) {
            TrianguloR l = (TrianguloR) figura;
            puntos = new Point[3];           
             puntos[0] = l.punto1;
            puntos[1] = l.punto2;
            puntos[2] = l.punto3;
        }
         if (figura instanceof Triangulo) {
            Triangulo l = (Triangulo) figura;          
             puntos = new Point[3];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
            puntos[2] = l.punto3;
        }
          if (figura instanceof Cuadrado) {
            Cuadrado l = (Cuadrado) figura;
            puntos = new Point[4];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
             puntos[2] = l.punto3;
            puntos[3] = l.punto4;
        }
            if (figura instanceof CuadradoR) {
            CuadradoR l = (CuadradoR) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
             puntos[2] = l.punto3;
            puntos[3] = l.punto4;
        }
            if (figura instanceof Circulo) {
            Circulo l = (Circulo) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
        }
        if (figura instanceof CirculoR) {
            CirculoR l = (CirculoR) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
        }
       
        if (figura instanceof Elipse) {
            Elipse l = (Elipse) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
        }
          if (figura instanceof ElipseR) {
            ElipseR l = (ElipseR) figura;
             puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;
        }
        if (figura instanceof Poligono) {
            Poligono l = (Poligono) figura;
            puntos = new Point[2];
            puntos[0] = l.punto1;
            puntos[1] = l.punto2;            
            la = l.lados;
            grados();
        }
//         if (figura instanceof PoligonoR) {
//            PoligonoR l = (PoligonoR) figura;            
//            puntos = new Point[2];
//            puntos[0] = l.punto1;
//            puntos[1] = l.punto2;            
//            la = l.lados;
//            
//        }
        return puntos;
    }
 
    public void guardarImagen() {

        // Leer el archivo de figuras, crear los objetos que corresponda
        // y agregarlos al arreglo aFiguras.
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("png", "png");
        chooser.setFileFilter(filter);

        String rutaGuardar = "";
        int returnVal = chooser.showSaveDialog(this);

        FileWriter fw = null;
        String linea = "";

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            rutaGuardar = chooser.getSelectedFile().getPath();

            if (!rutaGuardar.contains(".png")) {
                rutaGuardar = chooser.getSelectedFile().getPath() + ".png";
            }

            BufferedImage img = toBufferedImage(raster.toImage(this));
            try {
                File outputfile = new File(rutaGuardar);
                ImageIO.write(img, "png", outputfile);
            } catch (IOException e) {

            }

        }

    }

    public void abrirVectores() {
        // TODO add your handling code here:
        this.aFiguras.clear();

        // Leer el archivo de figuras, crear los objetos que corresponda
        // y agregarlos al arreglo aFiguras.
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "txt", "txt");
        chooser.setFileFilter(filter);
        String nombreArchivo = "";
        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            nombreArchivo = chooser.getSelectedFile().getPath();

            openVectors(nombreArchivo);

            this.redibujar();

        }

    }

    public void openVectors(String ruta) {
        try {
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String linea = "";

            while ((linea = br.readLine()) != null) {
                if (linea.startsWith("L")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Color color = Color.decode("#" + puntos[5].substring(2));

                    this.aFiguras.add(new Linea(p1, p2, color));
                    this.listModel.addElement(linea);
                }

                if (linea.startsWith("TR") && linea.substring(0, 2).equals("TR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Point p3 = new Point(Integer.parseInt(puntos[5]), Integer.parseInt(puntos[6]));
                    Color color = Color.decode("#" + puntos[7].substring(2));
                    
                    this.aFiguras.add(new TrianguloR(p1, p2, p3, color));
                    this.listModel.addElement(linea);
                }

                if (linea.startsWith("T") && linea.substring(0, 1).equals("T") && !linea.substring(0, 2).equals("TR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Point p3 = new Point(Integer.parseInt(puntos[5]), Integer.parseInt(puntos[6]));
                    Color color = Color.decode("#" + puntos[7].substring(2));

                    this.aFiguras.add(new Triangulo(p1, p2, p3, color));
                    this.listModel.addElement(linea);
                }

                if (linea.startsWith("CuR") && linea.substring(0, 3).equals("CuR") ) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4])); 
                    Point p3 = new Point(Integer.parseInt(puntos[5]), Integer.parseInt(puntos[6]));
                    Point p4 = new Point(Integer.parseInt(puntos[6]), Integer.parseInt(puntos[7]));  
                    Color color = Color.decode("#" + puntos[8].substring(2));

                    this.aFiguras.add(new CuadradoR(p1,p2,p3,p4, color));
                    this.listModel.addElement(linea);
                }
                
                 if (linea.startsWith("Cu") && linea.substring(0, 2).equals("Cu") && !linea.substring(0, 3).equals("CuR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));   
                     Point p3 = new Point(Integer.parseInt(puntos[5]), Integer.parseInt(puntos[6]));
                    Point p4 = new Point(Integer.parseInt(puntos[6]), Integer.parseInt(puntos[7]));  
                    Color color = Color.decode("#" + puntos[8].substring(2));

                    this.aFiguras.add(new Cuadrado(p1,p2,p3,p4, color));
                    this.listModel.addElement(linea);
                }
                 
                if (linea.startsWith("CiR") && linea.substring(0, 3).equals("CiR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Color color = Color.decode("#" + puntos[5].substring(2));

                    this.aFiguras.add(new CirculoR(p1, p2, color));
                    this.listModel.addElement(linea);
                }

                if (linea.startsWith("Ci") && linea.substring(0, 2).equals("Ci") && !linea.substring(0, 3).equals("CiR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Color color = Color.decode("#" + puntos[5].substring(2));

                    this.aFiguras.add(new Circulo(p1, p2, color));
                    this.listModel.addElement(linea);
                }

                if (linea.startsWith("ElR") && linea.substring(0, 3).equals("ElR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Color color = Color.decode("#" + puntos[5].substring(2));

                    this.aFiguras.add(new ElipseR(p1, p2, color));
                    this.listModel.addElement(linea);
                }
                
                  if (linea.startsWith("El") && linea.substring(0, 2).equals("El") && !linea.substring(0, 3).equals("ElR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    Color color = Color.decode("#" + puntos[5].substring(2));

                    this.aFiguras.add(new Elipse(p1, p2, color));
                    this.listModel.addElement(linea);
                }
                 
                  if (linea.startsWith("PoR") && linea.substring(0, 3).equals("PoR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    int lados = Integer.parseInt(puntos[5]);
                    int g = Integer.parseInt(puntos[6]);
                    Color color = Color.decode("#" + puntos[7].substring(2));

                    this.aFiguras.add(new PoligonoR(p1, p2,lados,g, color));
                    this.listModel.addElement(linea);
                }
                  
                  if (linea.startsWith("Po") && linea.substring(0, 2).equals("Po") && !linea.substring(0, 3).equals("PoR")) {
                    String[] puntos = linea.split(",");
                    Point p1 = new Point(Integer.parseInt(puntos[1]), Integer.parseInt(puntos[2]));
                    Point p2 = new Point(Integer.parseInt(puntos[3]), Integer.parseInt(puntos[4]));
                    int lados = Integer.parseInt(puntos[5]);
                    int g = Integer.parseInt(puntos[6]);
                    Color color = Color.decode("#" + puntos[7].substring(2));

                    this.aFiguras.add(new Poligono(p1, p2,lados,g, color));
                    this.listModel.addElement(linea);
                }
            }

            br.close();
        } catch (IOException ex) {
            Logger.getLogger(Pizarra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void redibujar() {
        Iterator itr = aFiguras.iterator();
        int val = aFiguras.size();
        for(int i = 0; i< val;i++) {                                  
            Figura f = (Figura) aFiguras.get(i);
            if (f instanceof Linea) {
                Linea l = (Linea) f;
                this.dibujarLinea(l.punto1, l.punto2, l.color);
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;             
                this.dibujarTrianguloR(t.punto1, t.punto2, t.punto3, t.color);
            }

            if (f instanceof Triangulo) {
                Triangulo t = (Triangulo) f;
                this.dibujarTriangulo(t.punto1, t.punto2, t.punto3, t.color);
            }

            if (f instanceof Cuadrado) {
                Cuadrado c = (Cuadrado)f;
                this.dibujarCuadrado(c.punto1, c.punto2, c.color);
            }
             if (f instanceof CuadradoR) {
                CuadradoR c = (CuadradoR) f;
                this.dibujarCuadradoR(c.punto1, c.punto2, c.color);
            }

            if (f instanceof CirculoR) {
                CirculoR c = (CirculoR) f;
                this.dibujarCirculoR(c.punto1, c.punto2, c.color);
            }

            if (f instanceof Circulo) {
                Circulo c = (Circulo) f;
                this.dibujarCirculo(c.punto1, c.punto2, c.color);
            }

            if (f instanceof Elipse) {
                Elipse c = (Elipse) f;
                this.dibujarElipse(c.punto1, c.punto2, c.color);
            }
            if (f instanceof ElipseR) {
                ElipseR c = (ElipseR) f;
                this.dibujarElipseR(c.punto1, c.punto2, c.color);
            }
             if (f instanceof Poligono) {
                Poligono c = (Poligono) f;                
                this.dibujarPoligonoRegular(c.punto1, c.punto2,c.lados,c.gr, c.color);
            }
              if (f instanceof PoligonoR) {
                PoligonoR c = (PoligonoR) f;
                this.dibujarPoligonoRegularR(c.punto1, c.punto2,c.lados,c.gr, c.color);
            }
        }
    }

    
    
    
    
    
    
    
    
    //---------------------------------------
    public void guardarVectores() {        
        FileWriter fw = null;
        String linea="";

        try {
            fw = new FileWriter("vectores.txt");
            for (int i=0;i<aFiguras.size();i++){
                Figura f = aFiguras.get(i);
                
                if (f instanceof Linea){
                    Linea l = (Linea)f;
                   
                    linea=String.format("L,%.0f,%.0f,%.0f,%.0f,%x\n",l.punto1.getX(),l.punto1.getY(),
                                                          l.punto2.getX(),l.punto2.getY(),
                                                          l.color.getRGB());                                        
                } 
                
                if (f instanceof TrianguloR){
                    TrianguloR t = (TrianguloR)f;                   
                    linea=String.format("TR,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,
                                                                  t.punto3.x,t.punto3.y,
                                                                  t.color);                       
                }
                  if (f instanceof Triangulo){
                    Triangulo t = (Triangulo)f;                   
                    linea=String.format("T,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,
                                                                  t.punto3.x,t.punto3.y,
                                                                  t.color);                       
                }
                
                if (f instanceof CuadradoR){
                     CuadradoR t = (CuadradoR)f;                   
                    linea=String.format("CuR,%d,%d,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y, 
                                                                  t.punto3.x,t.punto3.y,
                                                                  t.punto4.x,t.punto4.y, 
                                                                       t.color.getRGB());  
                }
                 if (f instanceof Cuadrado){
                     Cuadrado t = (Cuadrado)f;                   
                    linea=String.format("Cu,%d,%d,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,
                                                                   t.punto3.x,t.punto3.y,
                                                                  t.punto4.x,t.punto4.y,
                                                                       t.color.getRGB());  
                }
                   if (f instanceof CirculoR){
                     CirculoR t = (CirculoR)f;                   
                    linea=String.format("CiR,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,                                                                                                                                    
                                                                       t.color.getRGB());  
                }
                     if (f instanceof Circulo){
                     Circulo t = (Circulo)f;                   
                    linea=String.format("Ci,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,                                                                                                                                    
                                                                       t.color.getRGB());  
                }
                   if (f instanceof ElipseR){
                     ElipseR t = (ElipseR)f;                   
                    linea=String.format("ElR,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,                                                                                                                                    
                                                                       t.color.getRGB());  
                }
                   if (f instanceof Elipse){
                     Elipse t = (Elipse)f;                   
                    linea=String.format("El,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                                  t.punto2.x,t.punto2.y,                                                                                                                                    
                                                                       t.color.getRGB());  
                }
                   if (f instanceof PoligonoR){                       
                                            
                       PoligonoR t = (PoligonoR)f; 
                        linea=String.format("PoR,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                          t.punto2.x,t.punto2.y,t.lados,t.gr,                                                         
                                                          color.getRGB()); 
                    
                }
                    if (f instanceof Poligono){
                     Poligono t = (Poligono)f; 
                        linea=String.format("Po,%d,%d,%d,%d,%d,%d,%x\n",t.punto1.x,t.punto1.y,
                                                          t.punto2.x,t.punto2.y,t.lados,t.gr,                                                         
                                                          color.getRGB());
                    
                }
                fw.write(linea);
            }
        } catch (IOException ex) {
            Logger.getLogger(Pizarra2.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Pizarra2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }    
   
    public void clear() {	
        int s = raster.size();
        for (int i = 0; i < s; i++) {
            raster.pixel[i] ^= 0x00ffffff;
        }
        repaint();
        return;
    }    
    
    public void lineaSimple(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;

        raster.setPixel(pix, x0, y0);

        if (dx != 0) {
            float m = (float) dy / (float) dx;
            float b = y0 - m*x0;

            dx = (x1 > x0) ? 1 : -1;

            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                raster.setPixel(pix, x0, y0);
            }
        }
    }
    
    public void lineaMejorada(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dx = x1 - x0;  int dy = y1 - y0;
        raster.setPixel(pix, x0, y0);
        if (Math.abs(dx) > Math.abs(dy)) {     // inclinacion < 1
            float m = (float) dy / (float) dx; // calcular inclinacion
            float b = y0 - m*x0;
            dx = (dx < 0) ? -1 : 1;
            while (x0 != x1) {
                x0 += dx;
                raster.setPixel(pix, x0, Math.round(m*x0 + b));
            }
        } else {
            if (dy != 0) {                         // inclinacion >= 1
                float m = (float) dx / (float) dy; // Calcular inclinacion
                float b = x0 - m*y0;
                dy = (dy < 0) ? -1 : 1;
                while (y0 != y1) {
                    y0 += dy;
                    raster.setPixel(pix, Math.round(m*y0 + b), y0);
                }
            }
        }
    }

    public void lineFast(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dy = y1 - y0;  int dx = x1 - x0;  int stepx, stepy;
        if (dy < 0) { dy = -dy;  stepy = -raster.width; } else { stepy = raster.width; }
        if (dx < 0) { dx = -dx;  stepx = -1; } else { stepx = 1; }
        dy <<= 1;   dx <<= 1;
        y0 *= raster.width;  y1 *= raster.width;   raster.pixel[x0+y0] = pix;
        if (dx > dy) {
            int fraction = dy - (dx >> 1);
            while (x0 != x1) {
                if (fraction >= 0) {
                    y0 += stepy; fraction -= dx;
                }
                x0 += stepx;  fraction += dy;
                raster.pixel[x0+y0] = pix;
            }
        } else {
            int fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepx; fraction -= dy;
                }
                y0 += stepy; fraction += dx;
                raster.pixel[x0+y0] = pix;
            }
        }
    }
    
    private void dibujarLinea(Point _p1, Point _p2, Color color) {
             long inicio=0, fin=0;
             //inicio = System.nanoTime();
             lineaMejorada(_p1.x,_p1.y,_p2.x,_p2.y,color);
             //fin    = System.nanoTime();
             
             //System.out.printf("Tiempo transcurrido, simple: %d\n",(fin-inicio));
             
             //inicio = System.nanoTime();
            // lineFast(_p1.x,_p1.y,_p2.x,_p2.y,color);
             //fin    = System.nanoTime();            
             
             //System.out.printf("Tiempo transcurrido, fast  : %d\n",(fin-inicio));             
    }    
   
    
     private void dibujarTrianguloR(Point p1, Point p2, Point p3, Color c){
                // TODO add your handling code here:
        
        
        TrianguloR tri = new TrianguloR(p1,p2,p3,c);
                
        tri.dibujar(raster);
        
        aFiguras.add(tri);               
       

    }
    private void dibujarTriangulo(Point p1, Point p2, Point p3, Color c){
                // TODO add your handling code here:
        dibujarLinea(p3,p1,c);
        dibujarLinea(p1,p2,c);
        dibujarLinea(p2,p3,c);
        
        Triangulo tri = new Triangulo(p1,p2,p3,c);        aFiguras.add(tri);

                 

    }
     private void dibujarCuadradoR(Point p1, Point p2, Color c){
                // TODO add your handling code here:
    int aY = p2.y - p1.y;
        Point p0 = new Point(p1.x,p1.y + aY);
        Point p3 = new Point(p2.x,p2.y - aY);
                
        
         TrianguloR tri = new TrianguloR(p1,p0,p2,c);
             tri.dibujar(raster);
        
        aFiguras.add(tri);   
         tri = new TrianguloR(p1,p3,p2,c);

                tri.dibujar(raster);
        CuadradoR cu = new CuadradoR(p0,p1,p2,p3,c);
               
        aFiguras.add(cu);       
        
   

    }
       private void dibujarCuadrado(Point p1, Point p2, Color c){
                // TODO add your handling code here:
        p3 = new Point(p2.x, p1.y);
        p4 = new Point(p1.x, p2.y);
        lineFast(p1.x, p1.y, p3.x, p3.y, c);
        lineFast(p3.x, p3.y, p2.x, p2.y, c);
        lineFast(p2.x, p2.y, p4.x, p4.y, c);
        lineFast(p4.x, p4.y, p1.x, p1.y, c);      
        Cuadrado cu = new Cuadrado(p1,p2,p3,p4,c);
                
                aFiguras.add(cu);
        

    }
       public void dibujarCirculoR(Point p1, Point p2, Color c){
     
        int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2) ;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        for (int i = 1; i < 365 ; i++) {
            int x = (int)(radio*Math.cos(Math.toRadians((i-1)*-1)));
            int y = (int)(radio*Math.sin(Math.toRadians((i-1)*-1)));
            int x2 = (int)(radio*Math.cos(Math.toRadians(i*-1)));
            int y2 = (int)(radio*Math.sin(Math.toRadians(i*-1)));
                      
                Point _p0 = new Point(p2.x,p2.y);
                Point _p1 = new Point(p2.x + x,p2.y  + y);
                Point _p2 = new Point(p2.x + x2,p2.y + y2);
              TrianguloR tri = new TrianguloR(_p0,_p1,_p2,c);
             tri.dibujar(raster);
        
           
           }
        CirculoR ci = new CirculoR(p1,p2,c);
        aFiguras.add(ci);
       
       }
       
        public void dibujarCirculo(Point p1, Point p2, Color c){
     
        int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2) ;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        for (int i = 1; i < 365 ; i++) {
            int x = (int)(radio*Math.cos(Math.toRadians((i-1)*-1)));
            int y = (int)(radio*Math.sin(Math.toRadians((i-1)*-1)));
            int x2 = (int)(radio*Math.cos(Math.toRadians(i*-1)));
            int y2 = (int)(radio*Math.sin(Math.toRadians(i*-1)));
            
            Point p_1 = new Point(p2.x + x,p2.y + y);
            Point p_2 = new Point(p2.x + x2,p2.y + y2);
           dibujarLinea(p_1,p_2,c);      
           }
              Circulo ci = new Circulo(p1,p2,c);aFiguras.add(ci);
         
       }
       
       public void dibujarElipseR(Point p1, Point p2, Color c){
//           int rX = p2.x - p1.x, 
//            rY = p2.y - p1.y;
//            rY *= (rY < 0)? -1 : 1;
//            rX *= (rX < 0)? -1: 1;
//            Elipse(c,p1.x,p1.y,rX,rY);
//                
   
        int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2) ;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        for (int i = 1; i < 365 ; i++) {
            int x = (int)(dX*Math.cos(Math.toRadians((i-1)*-1)));
            int y = (int)(dY*Math.sin(Math.toRadians((i-1)*-1)));
            int x2 = (int)(dX*Math.cos(Math.toRadians(i*-1)));
            int y2 = (int)(dY*Math.sin(Math.toRadians(i*-1)));
            
            Point _p0 = new Point(p2.x,p2.y);
                Point _p1 = new Point(p2.x + x,p2.y  + y);
                Point _p2 = new Point(p2.x + x2,p2.y + y2);
              TrianguloR tri = new TrianguloR(_p0,_p1,_p2,c);
             tri.dibujar(raster);
        
        
         
           }
             ElipseR eli = new ElipseR(p1,p2,c);
             aFiguras.add(eli);
        
    
       }
       
          public void dibujarElipse(Point p1, Point p2, Color c){
//         int rX = p2.x - p1.x, 
//            rY = p2.y - p1.y;
//            rY *= (rY < 0)? -1 : 1;
//            rX *= (rX < 0)? -1: 1;
//            Elipse(c,p1.x,p1.y,rX,rY);
//                  
        int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2) ;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        for (int i = 1; i < 365 ; i++) {
            int x = (int)(dX*Math.cos(Math.toRadians((i-1)*-1)));
            int y = (int)(dY*Math.sin(Math.toRadians((i-1)*-1)));
            int x2 = (int)(dX*Math.cos(Math.toRadians(i*-1)));
            int y2 = (int)(dY*Math.sin(Math.toRadians(i*-1)));
            Point p_1 = new Point(p2.x + x,p2.y + y);
            Point p_2 = new Point(p2.x + x2,p2.y + y2);
           dibujarLinea(p_1,p_2,c);                                                                 
           }  
                Elipse eli = new Elipse(p1,p2,c);
                aFiguras.add(eli);
        
    
       }
          
      public void dibujarPoligonoRegularR(Point p1, Point p2,int lados,int grad, Color c){
          int lado = 0;
          String l = tfLados.getText();
       if(lados == 0)  lado = Integer.parseInt(l);
       else lado = lados;      
                                                                  
            int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2),
            avance = 360/lado;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        
        for (int i = 1; i <= lado ; i++) {
            int x = (int)(radio*Math.cos(Math.toRadians(-grad+(avance*i))));
            int y = (int)(radio*Math.sin(Math.toRadians(-grad+(avance*i))));
            int x2 = (int)(radio*Math.cos(Math.toRadians(-grad+(avance*(i+1)))));
            int y2 = (int)(radio*Math.sin(Math.toRadians(-grad+(avance*(i+1)))));
            
                 Point _p0 = new Point(p2.x,p2.y);
                Point _p1 = new Point(p2.x + x,p2.y  + y);
                Point _p2 = new Point(p2.x + x2,p2.y + y2);
               
              TrianguloR tri = new TrianguloR(_p0,_p1,_p2,c);
             tri.dibujar(raster);
        
          
           }
                PoligonoR pol = new PoligonoR(p1,p2,lado,grad,c);aFiguras.add(pol);
                la = lado;       
            }
                      
       
     
    public void dibujarPoligonoRegular(Point p1, Point p2,int lados,int grad, Color c){
       int lado = 0;
          String l = tfLados.getText();
       if(lados == 0)  lado = Integer.parseInt(l);
       else lado = lados; 
            int dX = p2.x - p1.x, 
            dY = p2.y - p1.y,
            cX = p1.x + (int)(dX/2),
            cY = p1.y + (int)(dY/2),
            avance = 360/lado;
           
        int radio = (int) Math.sqrt((dX*dX) + (dY*dY));
        // dibujarCirculo(p1.x,p1.y,radio,c);
        //CircleMidPoint(c,p1.x,p1.y,radio);
        
        for (int i = 1; i <= lado ; i++) {
            int x = (int)(radio*Math.cos(Math.toRadians(-grad+(avance*i))));
            int y = (int)(radio*Math.sin(Math.toRadians(-grad+(avance*i))));
            int x2 = (int)(radio*Math.cos(Math.toRadians(-grad+(avance*(i+1)))));
            int y2 = (int)(radio*Math.sin(Math.toRadians(-grad+(avance*(i+1)))));
            Point p_1 = new Point(p1.x + x,p1.y + y);
            Point p_2 = new Point(p1.x + x2,p1.y + y2);
           dibujarLinea(p_1,p_2,c);
          
             
           }
               Poligono pol = new Poligono(p1,p2,lado,grad,c);aFiguras.add(pol);  
               la = lado;
            
                      
        
     }
      
      
      
      
      
    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {                                     
        // TODO add your handling code here:
//        figura = this.rbLinea.isSelected()?LINEA:TRIANGULO;
                if(this.rbLinea.isSelected()) figura = LINEA;
                else if(this.rbTriang.isSelected()) figura = TRIANGULO;
                else if(this.rbTriangR.isSelected()) figura = TRIANGULOR;
                 else if(this.rbCuad.isSelected()) figura = CUADRADO;
                 else if(this.rbCuadR.isSelected()) figura = CUADRADOR;
                else if(this.rbCirc.isSelected()) figura = CIRCULO;
                else if(this.rbCircR.isSelected()) figura = CIRCULOR;
                else if(this.rbElip.isSelected()) figura = ELIPSE;
                else if(this.rbElipR.isSelected()) figura = ELIPSER;
                else if(this.rbPolig.isSelected()) figura = POLIGONO;
                else if(this.rbPoligR.isSelected()) figura = POLIGONOR;
                
       
        if (bP1 && bP2 && !bP3) {
            p3.x = evt.getX();
            p3.y = evt.getY();
            bP3 = true;

          
        }

        if (bP1 && !bP2) {
            p2.x = evt.getX();
            p2.y = evt.getY();
            bP2 = true;

            
        }
               
        if (bP1 && !bP2){
            p2.x=evt.getX();
            p2.y=evt.getY();
            bP2 = true;                       
            
//            dibujarLinea(p2,p2,color);
            
//            if(figura == LINEA){
//                
//                Linea l = new Linea(p1,p2, color); 
//                
//                String linea=String.format("L,%.0f,%.0f,%.0f,%.0f,%x\n",l.punto1.getX(),l.punto1.getY(),
//                                                          l.punto2.getX(),l.punto2.getY(),
//                                                          l.color.getRGB());
//                    
//                
//                aFiguras.add(l);
//                listModel.addElement(linea);            
//           }
        } 
        
        if (!bP1){
            p1.x=evt.getX();
            p1.y=evt.getY();
            bP1 = true;

            dibujarLinea(p1,p1,color);
        }         
        
        if(figura==LINEA && bP1 && bP2){
            dibujarLinea(p1,p2,color);
            bP1=false;bP2=false;bP3=false;bP4=false;
             Linea l = new Linea(p1,p2, color); 
            String linea=String.format("L,%.0f,%.0f,%.0f,%.0f,%x\n",l.punto1.getX(),l.punto1.getY(),
                                                          l.punto2.getX(),l.punto2.getY(),
                                                          l.color.getRGB());
            aFiguras.add(l);
            listModel.addElement(linea);
        }
        
        
        if(figura==TRIANGULO && bP1 &&bP2 &&bP3){
            System.out.println("Dibujando triangulo");
            dibujarTriangulo(p1,p2,p3,color);
            bP1=false;bP2=false;bP3=false;bP4=false;
            String linea=String.format("T,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),
                                                          p3.getX(),p3.getY(),
                                                          color.getRGB());
        
        listModel.addElement(linea);
        }  
        if(figura==TRIANGULOR && bP1 &&bP2 &&bP3){
            System.out.println("Dibujando triangulo");
            dibujarTrianguloR(p1,p2,p3,color);
            bP1=false;bP2=false;bP3=false;bP4=false;
             String linea=String.format("TR,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),
                                                          p3.getX(),p3.getY(),
                                                          color.getRGB());
        listModel.addElement(linea);
        }  
         if(figura==ELIPSE && bP1 && bP2){
            System.out.println("Dibujando elipse");
            dibujarElipse(p1,p2,color);
            bP1=false;bP2=false;bP3=false;bP4=false;
             String linea=String.format("El,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);
        }  
         if(figura==ELIPSER && bP1 && bP2){
            System.out.println("Dibujando elipse");
            dibujarElipseR(p1,p2,color);
            bP1=false;bP2=false;bP3=false;bP4=false;
             String linea=String.format("ElR,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);
        }  
         if(figura==CUADRADO && bP1 &&bP2 ){
            System.out.println("Dibujando CUADRO");
            dibujarCuadrado(p1,p2,color);  
             p3 = new Point(p2.x, p1.y);
             p4 = new Point(p1.x, p2.y);
            bP1=false;bP2=false;bP3=false;bP4=false;
            String linea=String.format("Cu,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(), p3.getX(),p3.getY(),
                                                          p4.getX(),p4.getY(),                                                         
                                                          color.getRGB());
            listModel.addElement(linea);
        } 
         if(figura==CUADRADOR && bP1 &&bP2 ){
            System.out.println("Dibujando CUADRO");
            dibujarCuadradoR(p1,p2,color); 
            p3 = new Point(p2.x, p1.y);
             p4 = new Point(p1.x, p2.y);
            bP1=false;bP2=false;bP3=false;bP4=false;
            String linea=String.format("CuR,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(), p3.getX(),p3.getY(),
                                                          p4.getX(),p4.getY(),                                                         
                                                          color.getRGB());
            listModel.addElement(linea);
        } 
         if(figura==CIRCULO && bP1 && bP2 ){
            System.out.println("Dibujando CIRCULO");
            dibujarCirculo(p1,p2,color);           
            bP1=false;bP2=false;bP3=false;bP4=false;
            String linea=String.format("Ci,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);
        } 
          if(figura==CIRCULOR && bP1 && bP2 ){
            System.out.println("Dibujando CIRCULO");
            dibujarCirculoR(p1,p2,color);           
            bP1=false;bP2=false;bP3=false;bP4=false;
              String linea=String.format("CiR,%.0f,%.0f,%.0f,%.0f,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);
        } 
           if(figura==POLIGONO && bP1 && bP2 ){
            System.out.println("Dibujando POLIGONO");
            dibujarPoligonoRegular(p1,p2,0,grados,color);           
            bP1=false;bP2=false;bP3=false;bP4=false;
             String linea=String.format("Po,%.0f,%.0f,%.0f,%.0f,%d,%d,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),la,grados,                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);
        } 
            if(figura==POLIGONOR && bP1 && bP2 ){
            System.out.println("Dibujando POLIGONO");
            dibujarPoligonoRegularR(p1,p2,0,grados,color);           
            bP1=false;bP2=false;bP3=false;bP4=false;
             String linea=String.format("PoR,%.0f,%.0f,%.0f,%.0f,%d,%d,%x\n",p1.getX(),p1.getY(),
                                                          p2.getX(),p2.getY(),la,grados,                                                          
                                                          color.getRGB());
        
              listModel.addElement(linea);             
        } 
    }  
    
    public void jPanel1KeyReleased(KeyEvent ke) {      
        
        if(ke.getKeyCode()==KeyEvent.VK_T){
            this.figura = 1;
        }

        if(ke.getKeyCode()==KeyEvent.VK_L){
            this.figura = 0;
        }        
    } 
     public void borrarFiguras() {
        this.aFiguras.clear();
        this.listModel.clear();
        //Borrando pixeles de figuras dibujadas
        this.raster.pixel = new int[raster.pixel.length];
        panelRaster.repaint();
    }                 
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Pizarra2 pizarra = new Pizarra2();
                pizarra.setVisible(true);
                
            }
        });
    }
/*
       ███ elipse ███
      //creacion de un tercer punto el cual sera nuestro lado menor
            int aY = p2.y - p1.y;
        //distancias menor y mayor
            int dX = p2.x - p1.x,dmY = (p2.y - aY) - (p1.y + aY),dY = p2.y - p1.y;
        //obteniendo la posicion del eje menor
            int x = (int)(dmY*Math.cos(Math.toRadians(-45)));
            int y = (int)(dmY*Math.sin(Math.toRadians(-45)));  
        Vertex2D v1 = new Vertex2D(p1.x,p1.y,c.getRGB());
        Vertex2D  v2 = new Vertex2D(p1.x,p1.y + aY,c.getRGB());
        Vertex2D v3 = new Vertex2D(p2.x,p2.y,c.getRGB());
        Vertex2D v4 = new Vertex2D(p1.x - x,p1.y - y ,c.getRGB());
        
         TrianguloR tri = new TrianguloR(v1,v2,v3,c);
             tri.dibujar(raster);
        
        aFiguras.add(tri);   
         tri = new TrianguloR(v1,v3,v4,c);

                tri.dibujar(raster);
        
        aFiguras.add(tri);           
        listModel.addElement("Elipse");
    */
    public void grados(){
        String txt = this.campoR.getText();
        if(txt.equals("")){
            grados = 90;
        }else
        grados = Integer.parseInt(txt) + 90;        
    }
} 

